#include "drvparts.h"

/*
 * 디버그
 */
#define DEBUG_DRVPARTS_MOTOR
#define DEBUG_DRVPARTS_SOLE

Motor_MTSRS350SE Motor;
Solenoid Sole;

/**
 * MTS-RS350SE 모터 클래스
 */
void Motor_MTSRS350SE::assignPins(uint8_t en, uint8_t dir, uint8_t pwm) {
	pin_en = en;
	pin_dir = dir;
	pin_pwm = pwm;
	
	pinMode(pin_en, OUTPUT);
	pinMode(pin_dir, OUTPUT);
	pinMode(pin_pwm, OUTPUT);
}

void Motor_MTSRS350SE::write(MOTOR_CMD cmd, uint8_t speed /*= 0*/) {
	_speed = speed;
	
	switch (cmd) {
	default:
	case (MOTOR_CMD::STOP):
		#ifdef DEBUG_DRVPARTS_MOTOR
		Serial.println("MOTOR STOP MOVING!!!");
		#endif
	
		digitalWrite(pin_en, HIGH);
		analogWrite(pin_pwm, speed);
			
		isMoving = false;
		isMovingCW = false;
		isMovingCCW = false;
		break;
	case (MOTOR_CMD::CW):
		// CW 방향으로 움직이지 않거나 CCW 방향으로 움직이고 있는 경우
		//if (!isMovingCW | isMovingCCW) {
			#ifdef DEBUG_DRVPARTS_MOTOR
			Serial.println("MOTOR MOVING CW!!!");
			#endif
			
			digitalWrite(pin_en, LOW);
			delay(1);
			digitalWrite(pin_dir, HIGH);
			delay(1);
			analogWrite(pin_pwm, speed);
			delay(1);
			
			isMoving = true;
			isMovingCW = true;
			isMovingCCW = false;
		break;
	case (MOTOR_CMD::CCW):
		// CCW 방향으로 움직이지 않거나 CW 방향으로 움직이고 있는 경우
		//if (!isMovingCCW | isMovingCW) {
			#ifdef DEBUG_DRVPARTS_MOTOR
			Serial.println("MOTOR MOVING CCW!!!");
			#endif
			
			digitalWrite(pin_en, LOW);
			delay(1);
			digitalWrite(pin_dir, LOW);
			delay(1);
			analogWrite(pin_pwm, speed);
			delay(1);
			
			isMoving = true;
			isMovingCW = false;
			isMovingCCW = true;
		break;
	}
}

uint8_t Motor_MTSRS350SE::getSpeed() {
	return _speed;
}

/**
 *  솔레노이드 클래스
 */
void Solenoid::assign(uint8_t lock, const LimitSwitchWindow *limSwWindow, const LimitSwitchSolenoid *limSwSole, const Interface_Motor* motor) {
	pin_lock = lock;
	
	_limSwWindow = limSwWindow;
	_limSwSole   = limSwSole;
	_motor       = motor;
	
	pinMode(pin_lock, OUTPUT);
}

void Solenoid::write(SOLE_CMD cmd, uint8_t speed = 0) {
	_speed = speed;
	
	switch (cmd) {
	default:
	case (SOLE_CMD::STOP):
		#ifdef DEBUG_DRVPARTS_SOLE
		Serial.println("SOLENOID STOP!!!");
		#endif
		
		_motor->write(MOTOR_CMD::STOP);
		
		isMoving = false;
		isMovingLeft = false;
		isMovingRight = false;
		break;
	case (SOLE_CMD::LEFT):
		// 솔레노이드가 도킹이 되어있지 않는 경우
		if (!(_limSwSole->isDocked)) {
			#ifdef DEBUG_DRVPARTS_SOLE
			Serial.println("SOLENOID MOVING LEFT!!!");
			#endif
			
			_motor->write(MOTOR_CMD::CCW, speed);
			
			isMoving = true;
			isMovingLeft = true;
			isMovingRight = false;
		} else {
			#ifdef DEBUG_DRVPARTS_SOLE
			Serial.println("SOLENOID STOP!!!");
			#endif
			
			_motor->write(MOTOR_CMD::STOP);
			
			isMoving = false;
			isMovingLeft = false;
			isMovingRight = false;
		}
		
		break;
	case (SOLE_CMD::RIGHT):
		// 솔레노이드가 오른쪽 끝에 도달하지 않은 경우
		if (!(_limSwSole->isEndRight)) {
			#ifdef DEBUG_DRVPARTS_SOLE
			Serial.println("SOLENOID MOVING RIGHT!!!");
			#endif
			
			_motor->write(MOTOR_CMD::CW, speed);
			
			isMoving = true;
			isMovingLeft = false;
			isMovingRight = true;
		} else {
			#ifdef DEBUG_DRVPARTS_SOLE
			Serial.println("SOLENOID STOP!!!");
			#endif
			
			_motor->write(MOTOR_CMD::STOP);
			
			isMoving = false;
			isMovingLeft = false;
			isMovingRight = false;
		}
		
		break;
	case (SOLE_CMD::LOCK):
		#ifdef DEBUG_DRVPARTS_SOLE
		Serial.println("SOLENOID LOCK!!!");
		#endif
		
		this->lock();
		break;
	case (SOLE_CMD::RELEASE):
		#ifdef DEBUG_DRVPARTS_SOLE
		Serial.println("SOLENOID RELEASE!!!");
		#endif
		
		this->release();
		break;
	}
}

uint8_t Solenoid::getSpeed() {
	return _speed;
}

void Solenoid::lock(void) {
	digitalWrite(pin_lock, LOW);
	isLocked = true;
}

void Solenoid::release(void) {
	digitalWrite(pin_lock, HIGH);
	isLocked = false;
}