#include "windowController.h"

WindowController WindowCTL;

void WindowController::assignObjects(const LimitSwitchWindow *limSwWindow, const LimitSwitchSolenoid *limSwSole, const Solenoid *sole) {
	_limSwWindow = limSwWindow;
	_limSwSole = limSwSole;
	_sole = sole;
}

void WindowController::open(uint8_t speed /*= MAX_SPEED*/) {
	Serial.print("OPEN");
	_commandEvent = true;
	_speed = speed;
	
	_command = WINDOWCTL_CMD::OPEN;
}

void WindowController::close(uint8_t speed /*= MAX_SPEED*/) {
	Serial.print("CLOSE");
	_commandEvent = true;
	_speed = speed;
	
	_command = WINDOWCTL_CMD::CLOSE;
}

void WindowController::stop(void) {
	Serial.print("STOP");
	_commandEvent = true;
	_speed = 0;
	
	_command = WINDOWCTL_CMD::STOP;
	
	// 혹시 모를 위험 상황에 대비하기 위하여 write를 거치지 않고 바로 정지
	_sole->write(SOLE_CMD::STOP);
}

void WindowController::goToDockArea() {
	_speed = MAX_SPEED;
	_command = (WINDOWCTL_CMD::DOCK);
	status = (WINDOWCTL_STATUS::DOCK);
	_reserveFinished = true;
}

void WindowController::write() {
	if (this->commandEvent())
		_commandEvent = false;
	
	switch (status) {
	default:
	case (WINDOWCTL_STATUS::IDLE):
		if ((_limSwWindow->isOpened) && (_command == (WINDOWCTL_CMD::OPEN)) ||
			((_limSwWindow->isClosed) && (_command == (WINDOWCTL_CMD::CLOSE))) ||
			(_limSwSole->isDocked && (_command == (WINDOWCTL_CMD::DOCK)))) {
			Serial.println("WINDOWCTL_FINISHED!!!");
			_isFinished = true;
			
			if (_sole->isMoving)
				_sole->write(SOLE_CMD::STOP);
			
			_lastCommand = _command;
			_command = WINDOWCTL_CMD::IDLE;
		} else if (_reserveFailed) {
			Serial.println("WINDOWCTL_FAILED!!!");
			
			_isFailed = true;
		} else if (this->isAttached()) {
			Serial.println("WINDOWCTL_IDLE -> OPEN or CLOSE!!!");
			
			if (_command == (WINDOWCTL_CMD::OPEN))
				status = WINDOWCTL_STATUS::OPEN;
			else if (_command == (WINDOWCTL_CMD::CLOSE))
				status = WINDOWCTL_STATUS::CLOSE;
		} else if (this->isFound()) {
			Serial.println("WINDOWCTL_IDLE -> ATTACH!!!");
			
			status = WINDOWCTL_STATUS::ATTACH;
		} else if (!(_limSwSole->isDocked)) {
			Serial.println("WINDOWCTL_IDLE -> DOCK!!!");
			
			status = WINDOWCTL_STATUS::DOCK;
		} else {
			Serial.println("WINDOWCTL_IDLE -> FIND!!!");
			
			status = WINDOWCTL_STATUS::FIND;
		}
		
		break;
	case (WINDOWCTL_STATUS::OPEN):
		if (_limSwWindow->isOpened) {
			Serial.println("WINDOWCTL_OPEN -> DOCK!!!");
			
			_reserveFinished = true;
			
			_sole->write(SOLE_CMD::STOP);
			
			status = WINDOWCTL_STATUS::DOCK;
		} else if (_limSwSole->isDocked) {
			Serial.println("WINDOWCTL_OPEN FAILED!!!");
			
			_reserveFailed = true;
		// 창문 열릴 때도 isAttached를 확인해서 현재 창문이 제대로 끌려가는지 확인하고 싶은데
		// 창문이 열리는 방향은 센서와 멀어지는 방향이라 항상 isAttached는 false를 반환.
		// 창문이 열리는 경우에는 그냥 잘 붙어있나 보다 생각해야 함.
		}/* else if (this->isAttached()) {
			if (!(_sole->isMovingLeft) || (_sole->getSpeed() != _speed)) {
				Serial.println("WINDOWCTL_OPEN MOVING LEFT!!!");
				
				_sole->write(SOLE_CMD::LEFT, _speed);
			}
		} else {
			Serial.println("WINDOWCTL_OPEN -> ATTACH!!!");
			
			status = WINDOWCTL_STATUS::ATTACH;
		} */ else {
			if (!(_sole->isMovingLeft) || (_sole->getSpeed() != _speed)) {
				Serial.println("WINDOWCTL_OPEN MOVING LEFT!!!");
				
				_sole->write(SOLE_CMD::LEFT, _speed);
			}
		}
		
		break;
	case (WINDOWCTL_STATUS::CLOSE):
		if (_limSwWindow->isClosed) {
			_reserveFinished = true;
			
			_sole->write(SOLE_CMD::STOP);
			
			status = WINDOWCTL_STATUS::DOCK;
		} /*else if (this->isAttached()) {
			if (!(_sole->isMovingRight) || (_sole->getSpeed() != _speed)) {
				Serial.println("WINDOWCTL_OPEN MOVING RIGHT!!!");
				
				_sole->write(SOLE_CMD::RIGHT, _speed);
			}
		} else {
			Serial.println("WINDOWCTL_CLOSE -> ATTACH!!!");
			
			status = WINDOWCTL_STATUS::ATTACH;
		}*/ else {
			if (!(_sole->isMovingRight) || (_sole->getSpeed() != _speed)) {
				_sole->write(SOLE_CMD::RIGHT, _speed);
			}
		}
		
		break;
	case (WINDOWCTL_STATUS::DOCK):
		if (_limSwSole->isDocked && !_reserveFinished) {
			Serial.println("WINDOWCTL_DOCK -> FIND!!!");
			
			status = WINDOWCTL_STATUS::FIND;
		} else if (_limSwSole->isDocked && _reserveFinished) {
			Serial.println("WINDOWCTL_DOCK -> IDLE!!!");
			
			_reserveFinished = false;
			
			status = WINDOWCTL_STATUS::IDLE;
		} else {
			_sole->write(SOLE_CMD::RELEASE);
			
			if (!(_sole->isLocked) && (!(_sole->isMovingLeft) || (_sole->getSpeed() != _speed))) {
				Serial.print(_sole->getSpeed()); Serial.print(" "); Serial.print(_speed); Serial.println(" WINDOWCTL_DOCKING!!!");
				
				_sole->write(SOLE_CMD::LEFT, _speed);
			}
		}
		
		break;
	case (WINDOWCTL_STATUS::ATTACH):
		if (this->isAttached()) {
			Serial.println("WINDOWCTL_ATTACH -> OPEN or CLOSE!!!");
			
			_sole->write(SOLE_CMD::STOP);
			
			if (_command == WINDOWCTL_CMD::OPEN)
				status = WINDOWCTL_STATUS::OPEN;
			else if (_command == WINDOWCTL_CMD::CLOSE)
				status = WINDOWCTL_STATUS::CLOSE;
		} else if (!(this->isFound())) {
			Serial.println("WINDOWCTL_ATTACH -> FIND!!!");
			
			status = WINDOWCTL_STATUS::FIND;
		} else {
			Serial.println("WINDOWCTL_ATTACH_LOCK!!!");
			
			if (!(_sole->isLocked))
				_sole->write(SOLE_CMD::LOCK);
		}
	
		break;
	case (WINDOWCTL_STATUS::FIND):
		if (this->isFound()) {
			Serial.println("WINDOWCTL_FIND -> ATTACH!!!");
			
			_isFinding = false;
			
			_sole->write(SOLE_CMD::STOP);
			
			status = WINDOWCTL_STATUS::ATTACH;
		} else if (_limSwSole->isEndRight) {
			Serial.println("WINDOWCTL_FIND -> DOCK (isEndRight)!!!");
			
			_isFinding = false;
			
			status = WINDOWCTL_STATUS::DOCK;
		} else if (!(this->isFinding()) && !(_limSwSole->isDocked)) {
			Serial.println("WINDOWCTL_FIND -> DOCK (isFinding & !isDocked)!!!");
			
			_isFinding = false;
			
			status = WINDOWCTL_STATUS::DOCK;
		} else {
			//Serial.println("WINDOWCTL_FINDING!!!");
			
			_isFinding = true;
			
			if (_sole->isLocked)
				_sole->write(SOLE_CMD::RELEASE);
			
			if (!(_sole->isMovingRight) && !(_sole->isLocked))
				_sole->write(SOLE_CMD::RIGHT, MAX_SPEED / 2);
		}
	
		break;
	}
	
}

bool WindowController::isAttached() {
	return ((_limSwSole->onWindow) && (_sole->isLocked)) ? true : false;
}

bool WindowController::isFound() {
	return (_limSwSole->onWindow) ? true : false;
}

bool WindowController::isRunning() {
	return _isRunning;
}

bool WindowController::isFinished() {
	return (_isFinished && (status == WINDOWCTL_STATUS::IDLE)) ? true : false;
}

bool WindowController::isFinding() {
	return _isFinding;
}

bool WindowController::commandEvent() {
	return _commandEvent;
}

void WindowController::clearEvent() {
	_isFinished = false;
}

WINDOWCTL_CMD WindowController::command() {
	return _command;
}

WINDOWCTL_CMD WindowController::lastCommand() {
	return _lastCommand;
}

WINDOWCTL_STATUS WindowController::getStatus() {
	return status;
}

bool WindowController::isWindowOpen() {
	return !(_limSwWindow->isClosed);
}