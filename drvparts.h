#pragma once

#if __cplusplus <= 199711L
  #error This library needs at least a C++11 compliant compiler
#endif

#include <Arduino.h>
#include "switch.h"

enum class MOTOR_CMD : uint8_t {
	STOP, CW, CCW
};

enum class SOLE_CMD : uint8_t {
	STOP, LEFT, RIGHT, LOCK, RELEASE
};

class Interface_Motor {
public:
	bool isMoving;
	bool isMovingCW;
	bool isMovingCCW;
	
	virtual void write(MOTOR_CMD, uint8_t = 0) = 0;
	virtual uint8_t getSpeed();
};

class Motor_MTSRS350SE : public Interface_Motor {
public:
	void assignPins(uint8_t, uint8_t, uint8_t);
	virtual void write(MOTOR_CMD, uint8_t = 0);
	virtual uint8_t getSpeed();
	
private:
	uint8_t _speed;
	
	uint8_t pin_en;
	uint8_t pin_dir;
	uint8_t pin_pwm;
};

class Solenoid {
public:
	bool isLocked;
	bool isMoving;
	bool isMovingLeft;
	bool isMovingRight;
	
	void assign(uint8_t, const LimitSwitchWindow*, const LimitSwitchSolenoid*, const Interface_Motor*);
	void write(SOLE_CMD, uint8_t = 0);
	uint8_t getSpeed();
	
private:
	uint8_t _speed;
	
	LimitSwitchWindow   *_limSwWindow;
	LimitSwitchSolenoid *_limSwSole;
	Interface_Motor     *_motor;

	uint8_t pin_lock;
	
	void lock(void);
	void release(void);
};

extern Motor_MTSRS350SE Motor;
extern Solenoid Sole;