#include "switch.h"

LimitSwitchWindow LimSwWindow;
LimitSwitchSolenoid LimSwSole;

/**
 * 창문 리미트 스위치 클래스
 */
void LimitSwitchWindow::assignPins(const uint8_t lim_left, const uint8_t lim_right, const uint8_t lim_int /*= 0*/) {
	pin_left = lim_left;
	pin_right = lim_right;
	pin_int = lim_int;
	
	pinMode(lim_left, INPUT_PULLUP);
	pinMode(lim_right, INPUT_PULLUP);
	
	/*
	if (lim_int) {
		attachInterrupt(digitalPinToInterrupt(lim_int), this->intAction(), RISING);
	}
	*/
}

void LimitSwitchWindow::read(void) {
	if ((!isOpened) && (digitalRead(pin_left) == SW_Active)) {
		isChanged = true;
		isOpened = true;
	} else if (digitalRead(pin_left) == SW_Active) {
		isOpened = true;
	} else {
		isOpened = false;
	}
	
	if ((!isClosed) && (digitalRead(pin_right) == SW_Active)) {
		isChanged = true;
		isClosed = true;
	} else if (digitalRead(pin_right) == SW_Active) {
		isClosed = true;
	} else {
		isClosed = false;
	}
}

void LimitSwitchWindow::clearEvent(void) {
	isChanged = false;
}

/**
 * 솔레노이드 리미트 스위치 클래스
 */
void LimitSwitchSolenoid::assignPins(uint8_t lim_dock, uint8_t lim_right, uint8_t lim_attach, uint8_t lim_int /*= 0*/) {
	pin_dock = lim_dock;
	pin_right = lim_right;
	pin_attach = lim_attach;
	pin_int = lim_int;
	
	pinMode(pin_dock, INPUT_PULLUP);
	pinMode(pin_right, INPUT_PULLUP);
	pinMode(pin_attach, INPUT_PULLUP);
}
 
void LimitSwitchSolenoid::read(void) {
	if((!isDocked) && (digitalRead(pin_dock) == SW_Active)) {
		isChanged = true;
		isDocked = true;
	} else if (digitalRead(pin_dock) == SW_Active) {
		isDocked = true;
	} else {
		isDocked = false;
	}
	
	if((!isEndRight) && (digitalRead(pin_right) == SW_Active)) {
		isChanged = true;
		isEndRight = true;
	} else if (digitalRead(pin_right) == SW_Active) {
		isEndRight = true;
	} else {
		isEndRight = false;
	}
	
	if((!onWindow) && (digitalRead(pin_attach) == SW_Active)) {
		isChanged = true;
		onWindow = true;
	} else if (digitalRead(pin_attach) == SW_Active) {
		onWindow = true;
	} else {
		onWindow = false;
	}
}
 
void LimitSwitchSolenoid::clearEvent(void) {
	isChanged = false;
}