#pragma once

#if __cplusplus <= 199711L
  #error This library needs at least a C++11 compliant compiler
#endif

#include <Arduino.h>

class Interface_Switch {
public:
	bool isChanged;

	virtual void read(void) = 0;
	virtual void clearEvent(void) = 0;
	
protected:
	const bool SW_Active = LOW;
};

class ControlSwitch : public Interface_Switch {
public:
	void assignPins(uint8_t, uint8_t, uint8_t, uint8_t = 0);
	virtual void read(void);
	virtual bool isChanged(void);
};

class LimitSwitchWindow : public Interface_Switch {
public:
	bool isChanged;
	bool isOpened;
	bool isClosed;

	void assignPins(uint8_t, uint8_t, uint8_t = 0);
	
	virtual void read(void);
	virtual void clearEvent(void);
private:
	uint8_t pin_left;
	uint8_t pin_right;
	uint8_t pin_int;
};

class LimitSwitchSolenoid : public Interface_Switch {
public:
	bool isChanged;
	bool isEndRight;
	bool isDocked;
	bool onWindow;

	void assignPins(uint8_t, uint8_t, uint8_t, uint8_t = 0);
	
	virtual void read();
	virtual void clearEvent(void);
private:
	uint8_t pin_dock;
	uint8_t pin_right;
	uint8_t pin_attach;
	uint8_t pin_int;
};

//extern Switch              CtlSw;
extern LimitSwitchWindow   LimSwWindow;
extern LimitSwitchSolenoid LimSwSole;