#include "sensor.h"
#include "switch.h"
#include "drvparts.h"
#include "windowController.h"
#include "msp.h"

/*
 * 기능 활성화
 */
#define EN_SENSOR_RAINDROP
#define EN_SENSOR_DUST
#define EN_COM_BLUETOOTH

/*
 * 디버그
 */
//#define DEBUG_SYSSTATUS
#define DEBUG_SYSSTATUS_SIMPLE
#define DEBUG_SENSORTMR
//#define DEBUG_TIMEDIFF
#define DEBUG_MSP_COMMAND_EVENT

/*
 * 핀 설정 및 전역 설정
 */
// 글로벌 상수
uint8_t const PIN_NULL = 0;
 
// 일반 조작 스위치 핀 (Deprecated)
/*
uint8_t const PIN_SW0    = 11;
uint8_t const PIN_SW1    = 12;
uint8_t const PIN_SW2    = 13;
uint8_t const PIN_SW_INT = 2;
*/

// 리미트 스위치 핀
uint8_t const PIN_LIMSW_WINDOW_LEFT  = 29;
uint8_t const PIN_LIMSW_WINDOW_RIGHT = 25;
//uint8_t const PIN_LIMSW_WINDOW_INT   = 27;

uint8_t const PIN_LIMSW_SOLE_DOCK    = 27;
uint8_t const PIN_LIMSW_SOLE_RIGHT   = 23;
uint8_t const PIN_LIMSW_SOLE_ATTACH  = 31;
//uint8_t const PIN_LIMSW_SOLE_INT     = 0;

// 솔레노이드 잠금 핀
uint8_t const PIN_SOLE_LOCK = 12;

// 센서 관련
uint8_t const PIN_SENSOR_RAINDROP_ANALOG = 7;
//uint8_t const PIN_SENSOR_RAINDROP_DIGITAL	// unused
uint8_t const SENSOR_RAINDROP = 0;
uint8_t const SENSOR_DUST     = 1;

// 모터 드라이버 핀
uint8_t const PIN_MOTORDRV_EN  = 7;
uint8_t const PIN_MOTORDRV_DIR = 6;
uint8_t const PIN_MOTORDRV_PWM = 5;

// 모터 관련 설정
uint8_t const GLOBAL_MOTOR_SPEED = 0xFF;

/*
 * 전역 구조체
 */
typedef struct _SENSORDATA {
	int currSensorData = 0;
	int prevSensorData = 0;
	int diffSensorData = 0;
	int dataState;
} SensorData;

/*
 * 통신용 전역 ENUM
 */
enum class SERIAL_CMD : uint8_t {
	WINDOWCTL_OPEN = 100,
	WINDOWCTL_CLOSE = 101,
	WINDOWCTL_STOP = 102,
	WINDOWCTL_BUSY = 103,
	WINDOWCTL_FINISH = 104,
	
	WINDOWCTL_STATUS = 1,
	WINDOWCTL_POS = 2,
	
	WINDOWCTL_MODE = 3,
	WINDOWCTL_SET_MODE = 4,
	
	SENSOR_DATA_STATUS_RAINDROP = 200,
	SENSOR_DATA_STATUS_DUST = 202,
	SENSOR_DATA_STATUS_GASLEAK = 204,
	SENSOR_DATA_RAW_RAINDROP = 201,
	SENSOR_DATA_RAW_DUST = 203, 
	SENSOR_DATA_RAW_GASLEAK = 205,
	
	REFRESH_DATA = 245
};
 
/*
 * 전역 구조체 및 변수 선언
 */
SensorData Sensor_info[2];
volatile uint8_t SensorIndex = 0;
MSPClass::CommandData mspData;

// 자동 모드 관련
bool WindowCTL_MODE = false;

// 시간 관련 변수
unsigned long CurrTime = 0;
unsigned long CurrTimeINT = 0;
unsigned long PrevTimeINT = 0;
unsigned long PrevTimeSensor = 0;
unsigned long PrevTimeLimSwWindow = 0;
unsigned long PrevTimeLimSwSole = 0;
unsigned long PrevTimeSysDebug = 0;
unsigned long PrevTimeWindowCTL = 0;

/*
 * 함수 선언
 */
void readSensor(uint8_t, SensorData*);

// 유틸리티 함수
bool timeDiff(unsigned long, unsigned long*, unsigned long);

void setup() {
	// put your setup code here, to run once:
	// 디버깅용 시리얼
	Serial.begin(9600);
	Serial.println("Start");

	// 창문 리미트 스위치 초기화
	LimSwWindow.assignPins(PIN_LIMSW_WINDOW_LEFT, PIN_LIMSW_WINDOW_RIGHT);
	
	// 솔레노이드 리미트 스위치 초기화
	LimSwSole.assignPins(PIN_LIMSW_SOLE_DOCK, PIN_LIMSW_SOLE_RIGHT, PIN_LIMSW_SOLE_ATTACH);
	
	// 현재 리미트 스위치 값 읽음
	LimSwWindow.read();
	LimSwSole.read();
	
	// 모터 초기화 (정지)
	Motor.assignPins(PIN_MOTORDRV_EN, PIN_MOTORDRV_DIR, PIN_MOTORDRV_PWM);
	Motor.write(MOTOR_CMD::STOP, 0);
	
	// 솔레노이드 초기화 (릴리즈)
	Sole.assign(PIN_SOLE_LOCK, &LimSwWindow, &LimSwSole, &Motor);
	//Sole.write(SOLE_CMD::RELEASE);
	
	// 창문 컨트롤러 초기화
	WindowCTL.assignObjects(&LimSwWindow, &LimSwSole, &Sole);
	if (!LimSwSole.isDocked) {
		WindowCTL.goToDockArea();
	}
	
	// 먼지 센서 시리얼
	DustSensor.beginSerial2(9600);
		
	// 블루투스 시리얼 CMD
	MSP.beginSerial1(115200);
}

void loop() {
	// 현재 루프 진입 시간 체크
	CurrTime = millis();
	
	// 스위치 루프
	if (timeDiff(CurrTime, &PrevTimeLimSwWindow, 30)) {
		LimSwWindow.read();
		LimSwSole.read();
	}
	
	// 창문 명령 수행
	if ((WindowCTL.commandEvent() || (WindowCTL.command() != WINDOWCTL_CMD::IDLE))) {
		//&& timeDiff(CurrTime, &PrevTimeWindowCTL, 50)) {
		//Serial1.print((WindowCTL.commandEvent())); Serial1.print(" "); Serial1.print(static_cast<uint8_t>(WindowCTL.getStatus())); Serial1.print("\r\n");
		WindowCTL.write();
	}
	
	// 윈도우 컨트롤러 완료 이벤트
	if (WindowCTL.isFinished()) {
		MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_FINISH));
		
		
		if (WindowCTL.isWindowOpen()) {
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_OPEN));
		} else {
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_CLOSE));
		}
		/*
		switch (WindowCTL.lastCommand()) {
		case (WINDOWCTL_CMD::OPEN):
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_OPEN));
			break;
		case (WINDOWCTL_CMD::CLOSE):
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_STOP));
			break;
		case (WINDOWCTL_CMD::STOP):
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_STOP));
			break;
		}
		*/
		WindowCTL.clearEvent();
	}
	
	// 창문 리미트 스위치 이벤트
	if (LimSwWindow.isChanged) {
		Serial.println("LIMSWWINDOW EVENT!!!");
		if (LimSwWindow.isOpened && Sole.isMovingLeft) {
			Sole.write(SOLE_CMD::STOP);
			Sole.write(SOLE_CMD::RELEASE);
		}
		
		if (LimSwWindow.isClosed && Sole.isMovingRight) {
			Sole.write(SOLE_CMD::STOP);
			Sole.write(SOLE_CMD::RELEASE);
		}
		
		LimSwWindow.clearEvent();
	}
	
	// 솔레노이드 리미트 스위치 이벤트
	if (LimSwSole.isChanged) {
		Serial.println("LIMSWSOLE EVENT!!!");
		if (LimSwSole.isDocked && Sole.isMovingLeft) {
			Sole.write(SOLE_CMD::STOP);
			Sole.write(SOLE_CMD::LOCK);
		} else if (LimSwSole.isDocked) {
			Sole.write(SOLE_CMD::LOCK);
		}
		
		if (LimSwSole.isEndRight && Sole.isMovingRight) {
			Sole.write(SOLE_CMD::STOP);
			Sole.write(SOLE_CMD::RELEASE);
		}
		
		if (LimSwSole.onWindow) {
		}
		
		LimSwSole.clearEvent();
	}
	
	// 시리얼 명령 이벤트
	if (MSP.available()) {
		mspData = MSP.retrieveCMD();
		
		#ifdef DEBUG_MSP_COMMAND_EVENT
		Serial.print(" MSP Available CMD: "); Serial.println(mspData.command);
		#endif
		
		switch (mspData.command) {
		case (static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_OPEN)):
			if (!WindowCTL.isRunning()) {
				WindowCTL.open();
			} else {
				MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_BUSY));
			}
			
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_CLOSE)):
			if (!WindowCTL.isRunning()) {
				WindowCTL.close();
			} else {
				MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_BUSY));
			}
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_STOP)):
			WindowCTL.stop();
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_STATUS)):
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_SET_MODE)):
			Serial.print("Set MODE: "); Serial.println(mspData.getUint8(mspData.data[0]));
			WindowCTL_MODE = mspData.getUint8(mspData.data[0]);;
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::SENSOR_DATA_STATUS_RAINDROP)):
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::SENSOR_DATA_STATUS_RAINDROP), 1, Sensor_info[SENSOR_RAINDROP].dataState);
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::SENSOR_DATA_STATUS_DUST)):
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::SENSOR_DATA_STATUS_DUST), 1, Sensor_info[SENSOR_DUST].dataState);
			break;
		case (static_cast<uint8_t>(SERIAL_CMD::REFRESH_DATA)):
			uint8_t writeData[4];
			
			writeData[1] = WindowCTL_MODE;
			writeData[1] = WindowCTL.isWindowOpen();
			writeData[2] = Sensor_info[SENSOR_RAINDROP].dataState;
			writeData[3] = Sensor_info[SENSOR_DUST].dataState;
			
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::REFRESH_DATA), sizeof(writeData) / sizeof(*writeData), writeData);
			
			break;
		}
	}
	
	// 시스템 상태
	if (timeDiff(CurrTime, &PrevTimeSysDebug, 1000)) {
		Serial.print("== System Status ");
		
		#ifndef DEBUG_SYSSTATUS_SIMPLE
		Serial.println(CurrTime);
		#endif
		
		#ifdef DEBUG_SYSSTATUS_SIMPLE
		Serial.print(" isOpened: "); Serial.print(LimSwWindow.isOpened); 
		Serial.print(" isClosed: "); Serial.print(LimSwWindow.isClosed);
		Serial.print(" isDocked: "); Serial.print(LimSwSole.isDocked);
		Serial.print(" onWindow: "); Serial.print(LimSwSole.onWindow);
		Serial.print(" isLocked: "); Serial.print(Sole.isLocked);
		Serial.print(" SoleIsEndRight: "); Serial.print(LimSwSole.isEndRight);
		Serial.print(" SoleSpeed: "); Serial.println(Sole.getSpeed());
		#endif
		
		#ifdef DEBUG_SYSSTATUS
		Serial.print("== System Status "); Serial.println(CurrTime);
		Serial.print(" Limit SW window Info:"); Serial.print(" isOpened: "); Serial.print(LimSwWindow.isOpened); 
		Serial.print(" isClosed: "); Serial.println(LimSwWindow.isClosed);
		Serial.print(" Limit SW Sole Info:"); Serial.print(" isEndRight: "); Serial.print(LimSwSole.isEndRight);
		Serial.print(" isDocked: "); Serial.print(LimSwSole.isDocked);
		Serial.print(" onWindow: "); Serial.println(LimSwSole.onWindow);
		
		Serial.print(" Motor info :"); Serial.print(" isMoving: "); Serial.print(Motor.isMoving);
		Serial.print(" isMovingCW: "); Serial.print(Motor.isMovingCW);
		Serial.print(" isMovingCCW: "); Serial.println(Motor.isMovingCCW);
		
		Serial.print(" Solenoid info:"); Serial.print(" isLocked: "); Serial.print(Sole.isLocked);
		Serial.print(" isMoving: "); Serial.print(Sole.isMoving);
		Serial.print(" isMovingLeft: "); Serial.print(Sole.isMovingLeft);
		Serial.print(" isMovingRight: "); Serial.println(Sole.isMovingRight);
		
		Serial.print(" Window CTL info:"); Serial.print(" status: "); Serial.print(static_cast<uint8_t>(WindowCTL.getStatus()));
		Serial.print(" isFinding: "); Serial.print(WindowCTL.isFinding());
		Serial.print(" isFound: "); Serial.print(WindowCTL.isFound());
		Serial.print(" isAttached: "); Serial.println(WindowCTL.isAttached());
		
		Serial.println(" == END System Status");
		#endif
	}
	
	if (WindowCTL_MODE) {
		if (Sensor_info[SENSOR_RAINDROP].dataState > 0) {
			if (!WindowCTL.isRunning()) {
				WindowCTL.close();
			}
		}
		
		if (Sensor_info[SENSOR_DUST].dataState > 0) {
			if (!WindowCTL.isRunning()) {
				WindowCTL.close();
			}
		}
	}
	
	// 센서 타이머
	if (timeDiff(CurrTime, &PrevTimeSensor, 1000)) {
		// 디버그 코드
		#ifdef DEBUG_SENSORTMR
		Serial.println("== Sensor DATA");
		Serial.print(" SensorIndex: "); Serial.println(SensorIndex);
		#endif
		
		readSensor(SensorIndex, &Sensor_info[SensorIndex]);

		// 디버그 코드
		#ifdef DEBUG_SENSORTMR
		Serial.print(" SensorDataPrev: "); Serial.print(Sensor_info[SensorIndex].prevSensorData); Serial.print(" ");
		Serial.print(" SensorDATA    : "); Serial.print(Sensor_info[SensorIndex].currSensorData); Serial.print(" ");
		Serial.print(" SensorDiff    : "); Serial.print(Sensor_info[SensorIndex].diffSensorData); Serial.print(" ");
		Serial.print(" SensorState   : "); Serial.println(Sensor_info[SensorIndex].dataState);
		#endif
		
		SensorIndex = (SensorIndex + 1) % (sizeof(Sensor_info) / sizeof(*Sensor_info));
	}
}

// 디버깅용 시리얼 입력 이벤트
void serialEvent() {
	Serial.println("== Serial Event occured!");

	char ch;

	while (Serial.available()) {
		ch = Serial.read();

		switch(ch) {
		case 'r':
			Sole.write(SOLE_CMD::LEFT, GLOBAL_MOTOR_SPEED);
			break;
		case 'c':
			Sole.write(SOLE_CMD::RIGHT, GLOBAL_MOTOR_SPEED);
			break;
		case 'f':
			Sole.write(SOLE_CMD::STOP);
			break;
		case 'l':
			Sole.write(SOLE_CMD::LOCK);
			break;
		case 'o':
			Sole.write(SOLE_CMD::RELEASE); 
			break;
		case 'a':
			Serial.println("WindowCTL OPEN!!!");
			WindowCTL.open();
			break;
		case 's':
			Serial.println("WindowCTL CLOSE!!!");
			WindowCTL.close();
			break;
		case 'd':
			Serial.println("WindowCTL STOP!!!");
			WindowCTL.stop();
			break;
		case 'q':
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_OPEN));
			break;
		case 'w':
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::WINDOWCTL_CLOSE));
			break;
		case 'e':
			uint8_t writeData[3];
			
			writeData[0] = (!(LimSwWindow.isOpened) && (LimSwWindow.isClosed)) ? false : true;
			writeData[1] = Sensor_info[SENSOR_RAINDROP].dataState;
			writeData[2] = Sensor_info[SENSOR_DUST].dataState;
			
			MSP.sendCMD(static_cast<uint8_t>(SERIAL_CMD::REFRESH_DATA), sizeof(writeData) / sizeof(*writeData), writeData);
		default:
			Serial.print("Serial Value: "); Serial.println(ch);
			Serial1.write(ch);
		}
	}
}

// 블루투스 시리얼 입력 이벤트
#ifdef EN_COM_BLUETOOTH
void serialEvent1() {
	unsigned char c;
	
	while(Serial1.available()) {
		c = Serial1.read();
		
		Serial.print(" "); Serial.print(c); Serial.print(" ");
		MSP.receiveCMD(c);
	}
	
	Serial.print("\n");
}
#endif

// 먼지 센서 시리얼 이벤트
#ifdef EN_SENSOR_DUST
void serialEvent2() {
	while(DustSensor.available()) {
		DustSensor.readCmd();
	}
}
#endif

/**
 * readSensor
 * @brief 센서 데이터 구조체에 센서 정보를 저장하는 함수
 * @params index        = 센서 선택
 *         *sensor_info = 저장할 센서 데이터 구조체
 */
void readSensor(uint8_t index, SensorData* sensor_info) {
	sensor_info->prevSensorData = sensor_info->currSensorData;
  
	switch (index) {
	#ifdef EN_SENSOR_RAINDROP
	case SENSOR_RAINDROP:
		sensor_info->currSensorData = analogRead(PIN_SENSOR_RAINDROP_ANALOG);
		sensor_info->diffSensorData = sensor_info->currSensorData - sensor_info->prevSensorData;
		
		if((sensor_info->diffSensorData < -400)) {
			sensor_info->dataState = 4;
		} else if ((sensor_info->diffSensorData < -300)) {
			sensor_info->dataState = 3;  
		} else if ((sensor_info->diffSensorData < -200)) {
			sensor_info->dataState = 2;
		} else if ((sensor_info->diffSensorData < -100)) {
			sensor_info->dataState = 1;
		} else {
			sensor_info->dataState = 0;
		}
	  
		break;
	#endif
	#ifdef EN_SENSOR_DUST
	case SENSOR_DUST:
		DustSensor.sendCmd();
		sensor_info->currSensorData = static_cast<int>(DustSensor.get_ug() * 10); // for float to int
		sensor_info->diffSensorData = sensor_info->currSensorData - sensor_info->prevSensorData;

		if((sensor_info->currSensorData / 10) > 300) {
			sensor_info->dataState = 2;
		} else if ((sensor_info->currSensorData / 10) > 150) {
			sensor_info->dataState = 1;
		} else {
			sensor_info->dataState = 0;
		}
		
		break;
	#endif
  }
}

/**
 * timeDiff
 * @brief 현재 아두이노 시간과 이전 시간을 비교 하여 그 차이가
 *        desired를 넘어서면 true 값 반환 및 이전 시간 최신화.
 * @params current = 현재 아두이노 시간
 *         *prev   = 이전 시간
 *         desired = 원하는 시간 간격
 */
bool timeDiff(unsigned long current, unsigned long *prev, unsigned long desired) {
	bool result = false;

	if ((current - *prev) > desired) {
		#ifdef DEBUG_TIMEDIFF
		Serial.println("=== timeDiff");
		Serial.print("Current Time: "); Serial.print(current); Serial.print(" ");
		Serial.print("Prev Time   : "); Serial.print(*prev); Serial.print(" ");
		Serial.print("Desired Time: "); Serial.print(desired); Serial.print(" ");
		Serial.print("Diff Time   : "); Serial.println(current - *prev);
		Serial.println("=== timeDiff");
		#endif
		 
		*prev = current;
		result = true;
	}

	return result;
}