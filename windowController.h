#pragma once

#if __cplusplus <= 199711L
  #error This library needs at least a C++11 compliant compiler
#endif

#include <Arduino.h>
#include "switch.h"
#include "drvparts.h"

enum class WINDOWCTL_CMD : uint8_t {
	IDLE, OPEN, CLOSE, STOP, DOCK
};

enum class WINDOWCTL_STATUS : uint8_t {
	IDLE, OPEN, CLOSE, FIND, ATTACH, STOP, DOCK
};

class WindowController {
private:
	WINDOWCTL_STATUS status = WINDOWCTL_STATUS::IDLE;
	
	bool _commandEvent = false;
	bool _isRunning = false;
	bool _isFinished = false;
	bool _isFinding = false;
	bool _isFailed = false;
	bool _reserveFailed = false;
	bool _reserveFinished = false;
	
	WINDOWCTL_CMD _command = WINDOWCTL_CMD::IDLE;
	WINDOWCTL_CMD _lastCommand = WINDOWCTL_CMD::IDLE;
	
	const LimitSwitchWindow* _limSwWindow;
	const LimitSwitchSolenoid* _limSwSole;
	const Solenoid* _sole;
	
	static const uint8_t MAX_SPEED = 0xFF;
	
	uint8_t _speed;
public:
	bool commandEvent();
	WINDOWCTL_CMD command();
	WINDOWCTL_CMD lastCommand();
	
	bool isRunning();
	bool isFinished();
	bool isFinding();
	
	bool isFailed();
	
	bool isFound();
	bool isAttached();

	bool isWindowOpen();
	
	WINDOWCTL_STATUS getStatus();
		
	void assignObjects(const LimitSwitchWindow *limSwWindow, const LimitSwitchSolenoid *limSwSole, const Solenoid *sole);
	void open(uint8_t speed = MAX_SPEED);
	void close(uint8_t speed = MAX_SPEED);
	void stop(void);
	void write(void);
	void goToDockArea();
	void clearEvent();
};

extern WindowController WindowCTL;