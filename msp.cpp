#include "msp.h"

MSPClass MSP;	

#define DEBUG_OUTPUT
#define DEBUG_OUTPUT_SIMPLE
//#define DEBUG_INPUT
#define DEBUG_INPUT_SIMPLE

void MSPClass::begin(unsigned long baudrate, uint8_t rx, uint8_t tx) {
	isHwSerial = false;
		
	swSerial = new SoftwareSerial(baudrate, rx, tx);
	
	#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE) || defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.println("Start MSP with SoftwareSerial");
	#endif
}

void MSPClass::beginSerial(unsigned long baudrate) {
	isHwSerial = true;
	
	hwSerial = &Serial;
	hwSerial->begin(baudrate);
	
	#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE) || defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.println("Start MSP with HardwareSerial");
	#endif
}

#if defined (__AVR_ATmega1280__) || defined (__AVR_ATmega128__) || defined (__AVR_ATmega2560__)
void MSPClass::beginSerial1(unsigned long baudrate) {
	isHwSerial = true;
	
	hwSerial = &Serial1;
	hwSerial->begin(baudrate);
	
	#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE) || defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.println("Start MSP with HardwareSerial1");
	#endif
}

void MSPClass::beginSerial2(unsigned long baudrate) {
	isHwSerial = true;
	
	hwSerial = &Serial2;
	hwSerial->begin(baudrate);
	
	#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE) || defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.println("Start MSP with HardwareSerial2");
	#endif
}

void MSPClass::beginSerial3(unsigned long baudrate) {
	isHwSerial = true;
	
	hwSerial = &Serial3;
	hwSerial->begin(baudrate);
	
	#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE) || defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.println("Start MSP with HardwareSerial3");
	#endif
}
#endif

void MSPClass::sendCMD(uint8_t cmd) {
	sendCMD(cmd, 0, 0);
}

void MSPClass::sendCMD(uint8_t cmd, size_t size, uint8_t *data) {
	uint8_t writeBuff[MSP_OUT_BUF_SIZE];
	uint8_t cursor = 0;
	
	// write PREAMBLE to buff
	for (size_t i = 0; i < strnlen(PREAMBLE, MSP_OUT_BUF_SIZE); i++) {
		writeBuff[cursor++] = *(PREAMBLE + i);
	}
	
	// write Direction to buff
	writeBuff[cursor++] = '<';
	// write Size to buff
	writeBuff[cursor++] = (uint8_t)size;
	// write Command to buff
	writeBuff[cursor++] = cmd;
	
	// write Data to buff
	for (size_t i = 0; i < size; i++) {
		writeBuff[cursor++] = *(data + i);
	}
	
	// write checksum to buff
	writeBuff[cursor++] = calcChecksum(cmd, (uint8_t)size, data);
	
	// write command to serial
	if (isHwSerial) {
		for (int i = 0; i < cursor; i++) {
			#if defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
			Serial.print(" "); Serial.print(writeBuff[i]);
			#endif
		
			hwSerial->write(writeBuff[i]);
		}
	} else {
		for (int i = 0; i < cursor; i++) {
			#if defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
			Serial.print(" "); Serial.print(writeBuff[i]);
			#endif
			
			swSerial->write(writeBuff[i]);
		}
	}
	#if defined(DEBUG_OUTPUT) || defined(DEBUG_OUTPUT_SIMPLE)
	Serial.print("\n");
	#endif
}

void MSPClass::receiveCMD(char ch) {
	switch (receiveStatus) {
	case (RECEIVE_STATUS::IDLE):
		#ifdef DEBUG_INPUT
		Serial.println("MSP STATUS IDLE");
		#endif
	
		receiveStatus = (ch == '$') ? (RECEIVE_STATUS::HEADER_START) : (RECEIVE_STATUS::IDLE);
		break;
	case (RECEIVE_STATUS::HEADER_START):
		#ifdef DEBUG_INPUT
		Serial.println("MSP STATUS HEADER_START");
		#endif
		
		receiveStatus = (ch == 'M') ? (RECEIVE_STATUS::HEADER_M) : (RECEIVE_STATUS::IDLE);
		break;
	case (RECEIVE_STATUS::HEADER_M):
		#ifdef DEBUG_INPUT
		Serial.println("MSP STATUS HEADER_M");
		#endif
		
		receiveStatus = (ch == '>') ? (RECEIVE_STATUS::HEADER_DIR) : (RECEIVE_STATUS::IDLE);
		break;
	case (RECEIVE_STATUS::HEADER_DIR):
		#ifdef DEBUG_INPUT
		Serial.print("MSP STATUS HEADER_DIR");
		Serial.print(" SIZE: "); Serial.println(ch, DEC);
		#endif
	
		comData[cntComData].size = ch;
		receiveStatus = RECEIVE_STATUS::HEADER_SIZE;
		break;
	case (RECEIVE_STATUS::HEADER_SIZE):
		#ifdef DEBUG_INPUT
		Serial.print("MSP STATUS HEADER_SIZE");
		Serial.print(" CMD: "); Serial.println(ch, DEC);
		#define DEBUG_FLAG_DATA_START
		#endif
	
		comData[cntComData].command = ch;
		inBufCurser = 0;
		receiveStatus = RECEIVE_STATUS::CMD;
		break;
	case (RECEIVE_STATUS::CMD):
		#if defined(DEBUG_INPUT) && defined(DEBUG_FLAG_DATA_START)
		Serial.print("MSP STATUS CMD DATA: ");
		#undef DEBUG_FLAG_DATA_START
		#endif
		#ifdef DEBUG_INPUT
		Serial.print(" "); Serial.println(ch, DEC);
		#endif
	
		if (inBufCurser > MSP_IN_BUF_SIZE) {
			receiveStatus = RECEIVE_STATUS::IDLE;
			inBufCurser = 0;
			break;
		} else if (inBufCurser > (comData[cntComData].size - 1)) {
			receiveStatus = RECEIVE_STATUS::DATA;
		} else {
			comData[cntComData].data[inBufCurser++] = ch;
			break;
		}
		
	case (RECEIVE_STATUS::DATA):
		#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE)
		Serial.print("MSP STATUS DATA");
		#endif
	
		if (!verifyChecksum(comData[cntComData].command, comData[cntComData].size, comData[cntComData].data, ch)) {
			#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE)
			Serial.println(" VERIFY CHECKSUM FAILED!");
			#endif
		} else {
			#if defined(DEBUG_INPUT) || defined(DEBUG_INPUT_SIMPLE)
			Serial.println(" MESSAGE RECEIVED!");
			#endif
			
			cntComData = (cntComData + 1) % (sizeof(comData) / sizeof(*comData));
		}
		receiveStatus = RECEIVE_STATUS::IDLE;
		break;
	}
}

uint8_t MSPClass::available() {
	#ifdef DEBUG_INPUT
	Serial.print("num Received data: "); Serial.println(cntComData);
	#endif
	
	return cntComData;
}

MSPClass::CommandData MSPClass::retrieveCMD() {
	if (cntComData) {
		return comData[--cntComData];
	} else {
		return comData[0];
	}
}

/*
 * Protected methods
 */

uint8_t MSPClass::calcChecksum (uint8_t cmd, size_t size, uint8_t *data) {
	uint8_t checksum = 0;
	
	checksum ^= size;
	checksum ^= cmd;
	
	for (size_t i = 0; i < size; i++) {
		checksum ^= *(data + i);
	}
	
	return checksum;
}

bool MSPClass::verifyChecksum (uint8_t cmd, size_t size, uint8_t *data, uint8_t checksum) {
	return (calcChecksum(cmd, size, data) == checksum) ? true : false;
}